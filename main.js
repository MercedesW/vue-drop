Vue.component('drop-menu', {
    template: `
    <div class="menu">
        <ul>
            <li>Opcion1</li>
            <li>Opcion2</li>
            <li>Opcion3</li>
            <li>Opcion4</li>
        </ul>
    </div>`
})


var app5 = new Vue({
    el: '#app-5',
    data: {
        resultado: false,
    },
    methods: {
        showMenu() {
            this.resultado = !this.resultado
        },
    }
  })